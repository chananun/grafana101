-- CREATE DB
create database exercises;
\c exercises

-- CREATE TABLE
CREATE TABLE t_oil (
	region      text,
	country     text,
	year        int,
	production  int,
	consumption int
);

-- insert DATA
\copy t_oil FROM PROGRAM 'curl https://www.cybertec-postgresql.com/secret/oil_ext.txt';
